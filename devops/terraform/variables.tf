variable "namespace" {
    default = "default"
}

variable "app_name" {
    default = "node-app"
}

variable "config_path" {
    default = "~/.kube/config"
}