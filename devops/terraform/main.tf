terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "1.13.3"
    }
  }
}

provider "kubernetes" {
    config_path = var.config_path
}

resource "kubernetes_role" "rbac-ns" {
  metadata {
    name = "pods-app"
    namespace = var.namespace
    labels = {
      app = var.app_name
    }
  }

  rule {
    api_groups     = [""]
    resources      = ["pods"]
    verbs          = ["get", "list", "watch"]
  }
}

resource "kubernetes_role_binding" "rbac-ns-bind" {
  metadata {
    name      = "pods-app-bind"
    namespace = var.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "pods-app"
  }
  subject {
    kind      = "Group"
    name      = "dev-team"
    api_group = "rbac.authorization.k8s.io"
  }
}