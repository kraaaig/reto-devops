# Reto DevOps - Claudio Lara

Este repositorio contiene el desarrollo del [reto-devops](https://gitlab.com/clm-public/reto-devops) de CLM.

Descripcion de plataforma usada para desarrollar este reto:
```
Workstation: Ubuntu 20.04.1 LTS
Docker: 19.03.8
Docker-compose: 1.25.0
Kubernetes: 19.4 (using minikube v1.15.0)
Helm: 3.4.1
Terraform: 0.13.5
```

## Dockerize

La imagen docker que contiene la app NodeJS se encuentra publicada [aquí](https://hub.docker.com/repository/docker/kraaaig/node_app/general).

```
docker pull kraaaig/node_app:v1
```

La imagen docker se basó en la imagen `node:14-alpine`.

## Docker compose

El archivo [docker-compose](devops/docker-compose/docker-compose.yml) usa la imagen anteriormente mencionada, ademas de levantar un reverse-proxy usando Traefik. Decidí usar Traefik ya que es una alternativa super liviana y facil de configurar. Puede manejar TLS (con Let'sencypt) y realizar autodiscover de contenedores en un servidor con Docker.

## CI/CD

Para la integracion y build de la app, se generó el archivo [gitlab-ci.yml](.gitlab-ci.yml). Personalmente no tengo experiencia con esta herramienta de integracion, pero leyendo la documentacion, creé el archivo para realizar el build y ejecutar tests de la aplicacion.

## Kubernetes

El archivo [deployment.yml](devops/kubernetes/deployment.yml) contiene las configuraciones necesarias para desplegar en Kubernetes.

## Helm

La carpeta `devops/chart/node-app` contiene el chart generado para desplegar la aplicacion en Kubernetes, usando Ingress con TLS (autofirmado) y autenticacion basica para el endpoint` /private`.

## Terraform

El archivo [main.tf](devops/terraform/main.tf) contiene las configuraciones para el modulo que genera un objeto ripo Role con su correspondiente RoleBind.

## Makefile

Este punto no fue desarrollado ya que desconozco y no tengo experiencia con esta forma de automatizar todo lo anterior.