FROM node:14-alpine AS base
LABEL maintainer="Claudio Lara - claudio.ariel.lara@gmail.com"
LABEL dockerfile-version="v1.0"

RUN addgroup -g 1001 -S jon && adduser -u 1001 -S jon -G jon -D
RUN mkdir /opt/temp && chown -R jon:jon /opt/temp

USER jon
WORKDIR /opt/temp

COPY . .

EXPOSE 3000
CMD ["node", "index.js"]